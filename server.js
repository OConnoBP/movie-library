//  Environment variables
require('dotenv').config();

//  Libraries
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const AWS = require('aws-sdk');

//  AWS configuration
let aws = AWS;
aws.config.update({
  region: process.env.REGION,
  endpoint: `dynamodb.${process.env.REGION}.amazonaws.com`
});
module.exports.AWS = aws;

//  Express configuration
let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'dist/movie-library')));
app.use('/api', require('./server/routes'));
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/movie-library/index.html'));
});
const port = process.env.PORT || '3000';
app.set('port', port);

//  Server configuration
const server = http.createServer(app);
server.listen(port, () => {
  console.log(`API running on localhost:${port}`);
});


