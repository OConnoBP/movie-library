let server = require('../../server');

let express = require('express');
let router = express.Router();

let docClient = new server.AWS.DynamoDB.DocumentClient();

router.get('/getAllMovies', (req, res) => {
  let params = {
    TableName: 'Movie'
  };

  docClient.scan(params, (err, data) => {
    if (err) {
      res.send({status: 500, data: err});
      //res.status(500).json(err);
    } else {
      res.send({status: 200, data: data.Items});
      //res.status(200).json(data.Items);
    }
  });
});

module.exports = router;
