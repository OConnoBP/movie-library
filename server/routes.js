let express = require('express');
let router = express.Router();

router.use('/movies', require('./movies/movie'));

module.exports = router;
