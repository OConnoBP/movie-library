import {AfterContentInit, Directive, EventEmitter, Output} from '@angular/core';

@Directive({
  selector: '[appAfterIf]'
})
export class AfterIfDirective implements AfterContentInit {
  @Output('appAfterIf')
  public after: EventEmitter<AfterIfDirective> = new EventEmitter();

  public ngAfterContentInit(): void {
    setTimeout(() => {
      this.after.next(this);
    });
  }
}
