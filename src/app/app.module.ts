import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LibraryComponent } from './modules/library/library.component';
import { MovieDetailsComponent } from './modules/movie-details/movie-details.component';
import { NavbarComponent } from './modules/navbar/navbar.component';
import { LibrarySectionComponent } from './modules/library-section/library-section.component';
import { HttpClientModule } from '@angular/common/http';
import {MovieService} from './movie.service';
import { ErrorComponent } from './modules/error/error.component';
import { AfterIfDirective } from './after-if.directive';

@NgModule({
  declarations: [
    AppComponent,
    LibraryComponent,
    MovieDetailsComponent,
    NavbarComponent,
    LibrarySectionComponent,
    ErrorComponent,
    AfterIfDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [MovieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
