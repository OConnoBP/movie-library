export class MovieDetails {
  Image: string;
  Trailer: string;
  Description: string;
  Score: number;
  Rating: string;
  Runtime: string;
  Year: string;
  Cast: any;
  Director: any;
  Genre: any;
}
