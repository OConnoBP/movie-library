import {MovieDetails} from './movie-details';

export class Movie {
  Title: string;
  Details: MovieDetails;
}
