import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {Movie} from 'src/app/models/movie';
import * as $ from 'jquery';

@Component({
  selector: 'app-library-section',
  templateUrl: './library-section.component.html',
  styleUrls: ['./library-section.component.css']
})
export class LibrarySectionComponent implements OnInit, AfterViewInit {

  //  Component inputs
  @Input()section_movies: any;
  @Input()section_title: string;

  //  Component outputs
  @Output()selected_movie: EventEmitter<Movie> = new EventEmitter();

  constructor() {

  }

  ngOnInit() {
    window.addEventListener('resize', this.updateLibrarySectionArrowsVisibility.bind(this));
  }

  ngAfterViewInit() {
    //  Fix this. UiKit class doesn't get applied soon enough so I added a timeout.
    setTimeout(() => {
      this.updateLibrarySectionArrowsVisibility();
    }, 1000);
  }

  /**
   * This method handles when a movie from the library is clicked on.
   * @param selected_movie The movie selected from the library
   */
  handleMovieClicked(selected_movie: Movie) {
    this.selected_movie.emit(selected_movie);
  }

  updateLibrarySectionArrowsVisibility() {
    let hide_navigation_arrows = true;

    const library_section = $(`app-library-section[id="${this.section_title}Section"]`);
    //  Iterate through each movie to determine if it contains the "uk-active" class
    $(library_section).find('[class*="movie"]').each((movie_key, movie) => {
      if (!$(movie).hasClass('uk-active')) {
        //  If the movie does not contain the uk-active class, that means there are more movies than are shown on the
        //    screen, so the arrows shouldn't be hidden
        hide_navigation_arrows = false;
        return false;
      }
    }, hide_navigation_arrows);

    //  If no movies could be found that didn't have the "uk-active" class, that means they're all displayed and the
    //    navigation arrows can be hidden. Otherwise, they should be shown.
    if (hide_navigation_arrows === true) {
      $(library_section).find('[class*="library-navigator"]').hide();
    } else {
      $(library_section).find('[class*="library-navigator"]').show();
    }
  }
}
