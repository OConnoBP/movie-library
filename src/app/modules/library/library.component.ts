import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Movie } from 'src/app/models/movie';
import * as _ from 'underscore/underscore-min';
import { HttpClient } from '@angular/common/http';
import { MovieService } from '../../movie.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css']
})
export class LibraryComponent implements OnInit {

  all_movies: any;
  superhero_movies: any;
  horror_movies: any;
  animated_movies: any;
  searched_movies: any;

  @ViewChild('queryForm') query_form: ElementRef;
  @ViewChild('movieDetailsModal') movie_details_modal: ElementRef;

  selected_movie: Movie;

  //  Boolean that determines whether or not to load the movie-details component
  show_movie_details: boolean;

  //  Boolean that determines whether or not the user has performed a custom search
  custom_search: boolean;

  //  Boolean that determines whether or not the server could be contacted
  failed_to_load: boolean;
  movies_loaded: boolean;
  constructor(private http: HttpClient, private movie_service: MovieService) {
    this.searched_movies = [];

    //  Initialize flag values
    this.show_movie_details = false;
    this.custom_search = false;
    this.failed_to_load = false;
    this.movies_loaded = false;
  }

  ngOnInit() {
    //  Load the movies
    this.movie_service.getMovies().subscribe((res: any) => {
      if (res.status !== 200) {
        this.failed_to_load = true;
      } else {
        this.all_movies = res.data;
        this.superhero_movies = this.findMoviesByGenre('Superhero');
        this.horror_movies = this.findMoviesByGenre('Horror');
        this.animated_movies = this.findMoviesByGenre('Animated');
        this.movies_loaded = true;
        //  this.updateLibrarySectionArrowsVisibility();
      }
    });

    //  Add event listeners
    this.query_form.nativeElement.addEventListener('keyup', this.handleMovieSearch.bind(this));
    this.movie_details_modal.nativeElement.addEventListener('hidden', this.handleSelectedMovieUnload.bind(this));
  }

  //////////////////////////////////////////////////////
  //                 Event Handlers                   //
  //////////////////////////////////////////////////////

  /**
   * This method is an event handler for when the movie details modal is opened
   * @param selected_movie A movie object that contains information on the currently selected movie
   */
  handleSelectedMovieLoad(selected_movie: Movie) {
    this.selected_movie = selected_movie;
    this.show_movie_details = true;
  }

  /**
   * This method is an event handler for when the movie details modal is closed
   */
  handleSelectedMovieUnload() {
    this.show_movie_details = false;
  }

  /**
   * This method is an event handler for when the user performs a custom search
   */
  handleMovieSearch() {
    let queried_movies = [];

    if (this.query_form.nativeElement[0].value !== '') {
      queried_movies = this.findMoviesByTitle(this.query_form.nativeElement[0].value);

      this.searched_movies = queried_movies;
      this.custom_search = true;
    } else {
      this.searched_movies = undefined;
      this.custom_search = false;
    }
  }

  //////////////////////////////////////////////////////
  //              End of Event Handlers               //
  //////////////////////////////////////////////////////

  findMoviesByTitle(query: string) {
    const queried_movies = [];

    this.all_movies.forEach((movie) => {
      if (movie.Title.toLowerCase().includes(query.toLowerCase())) {
        queried_movies.push(movie);
      }
    });

    return queried_movies;
  }

  findMoviesByGenre(genre: string) {
    const queried_movies = [];

    this.all_movies.forEach((movie) => {
      if (movie.Details.Genre.values.includes(genre)) {
        queried_movies.push(movie);
      }
    });

    return queried_movies;
  }
}
