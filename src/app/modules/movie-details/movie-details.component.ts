import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {Movie} from 'src/app/models/movie';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {

  //  The component's inputs
  @Input()movie: Movie;

  //  HTML elements within the component as variables
  @ViewChild('trailerIframe') iframe: ElementRef;

  trailer_loaded: boolean;

  constructor(public sanitizer: DomSanitizer) {
    //  Set variable defaults
    this.trailer_loaded = false;
  }

  ngOnInit() {
    //  Add event listeners
    this.iframe.nativeElement.addEventListener('load', this.handleTrailerLoaded.bind(this), { once: true });
  }

  /**
   * Generates a trusted url for the trailer and returns it
   */
  getSafeTrailerUrl() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(`${this.movie.Details.Trailer}?rel=0&amp;controls=0&amp;showinfo=0&amp;`);
  }

  /**
   * Handler function for when the trailer is loaded
   */
  handleTrailerLoaded() {
    this.trailer_loaded = true;
  }
}
